# Program: L6470.py
# Author: Adam Fuller
# Date 26/8/2015
# Description: Control an L6470 chip (on a Sparkfun Autodriver board) from a
# Beaglebone Black running Linux and Python via SPI.  Sparkfun have put
# together a C library for doing essentially exactly that on an Arduino, but I
# couldn't find anything for Python.  The feature of this module is the
# connection class which you instantiate and then whose methods you call to
# read information from or send commands to the L6470 chip.
#
# Usage:
# import L6470
# c = L6470.connection(0,0) # Instantiate an SPI connection to /dev/spidev1.0.
# c.Move(10000) # Use methods of the connection instance to make the motor go.
# ...
# # do cool stuff.
# ...
# c.close() # Because the connection object creates threads, call the close
# # method prior to shutting down Python.
#
# Reference: "L6470 dSPIN fully integrated microstepping motor driver with
# motion engine and SPI" (ST document 16737 Rev 3)

import time
from math import pi
from random import random

from Adafruit_BBIO.SPI import SPI
import Adafruit_BBIO.GPIO as GPIO

import threading
from multiprocessing import Value
from collections import deque

# See Table 37 "Application commands".
_commands = {
	'Run': 0b01010000, # last bit is direction
	'Move': 0b01000000, # last bit is direction
	'GoTo': 0b01100000,
	'GoTo_DIR': 0b01101000, # last bit is direction
	'GetParam': 0b00100000,
	'GetStatus': 0b11010000,
	'ResetDevice': 0b11000000,
	'ResetPos': 0b11011000,
	'SoftStop': 0b10110000,
	'HardStop': 0b10111000,
	'SoftHiZ':  0b10100000,
	'HardHiZ':  0b10101000,
	}

# See Table 35 STATUS register DIR bit.
_DIR = {'Forward': 1, 'Reverse': 0}
_DIRinv = {v: k for k,v in _DIR.iteritems()}

# See Table 9 "Registers map".
_address = {
	'ABS_POS': 0x1,
	'EL_POS': 0x2,
	'MARK': 0x3,
	'SPEED': 0x4,
	'ACC': 0x5,
	'DEC': 0x6,
	'MAX_SPEED': 0x7,
	'MIN_SPEED': 0x8,
	'FS_SPD': 0x15,
	'KVAL_HOLD': 0x9,
	'KVAL_RUN': 0xa,
	'KVAL_ACC': 0xb,
	'KVAL_DEC': 0xc,
	'INT_SPD': 0xd,
	'ST_SLP': 0xe,
	'FN_SLP_ACC': 0xf,
	'FN_SLP_DEC': 0x10,
	'K_THERM': 0x11,
	'ADC_OUT': 0x12,
	'OCD_TH': 0x13,
	'STALL_TL': 0x14,
	'STEP_MODE': 0x16,
	'ALARM_EN': 0x17,
	'CONFIG': 0x18,
	'STATUS': 0x19,
	'RESERVED1': 0x1a,
	'RESERVED2': 0x1b,
	}

# See Table 26 Programmable power bridge output slew-rate values.
_POW_SR = {180: 1, 290: 0b10, 530: 0b11}

# See Table 23 Oscillator management.
_EXT_CLK_and_OSC_SEL = {
	'internal, no output': 0b0000,
	'internal, 2 MHz output': 0b1000,
	'internal, 4 MHz output': 0b1001,
	'internal, 8 MHz output': 0b1010,
	'internal, 16 MHz output': 0b1011,
	'external 8 MHz crystal': 0b0100,
	'external 16 MHz crystal': 0b0101,
	'external 24 MHz crystal': 0b0110,
	'external 32 MHz crystal': 0b0111,
	'external 8 MHz clock': 0b0100,
	'external 16 MHz clock': 0b1101,
	'external 24 MHz clock': 0b1110,
	'external 32 MHz clock': 0b1111,
}

# See Table 25 Overcurrent event.
_OC_SD = {
	'enable over-current shutdown': 1,
	'disable over-current shutdown': 0,
}

# See Table 27 Motor supply voltage compenstation enable.
_EN_VSCOMP = {
	'enable supply voltage variation compensation': 1,
	'disable supply voltage variation compensation': 0,
}

# See Table 24 External switch hard stop interrupt mode.
_SW_MODE = {
	'User-defined': 1,
	'HardStop': 0,
}

# See Table 29 PWM frequency integer division factor.
_F_PWM_INT = {
	1: 0b0,
	2: 0b1,
	3: 0b10,
	4: 0b11,
	5: 0b100,
	6: 0b101,
	7: 0b110,
	8: 0b111,
}

# See Table 29 PWM frequency multiplication factor.
_F_PWM_DEC = {
	0.625: 0b0,
	0.75: 0b1,
	0.875: 0b10,
	1: 0b11,
	1.25: 0b100,
	1.5: 0b101,
	1.75: 0b110,
	2: 0b111,
}
def _bits_to_int(l):

	"""
	Given a list of ints in [0,1], MSB first, shift and add and return the
	resulting integer.
	"""

	assert all(map(lambda x: x in [0,1], l))

	return reduce(lambda x,y: (x<<1)+y, l)

def _bytes_to_int(l):

	"""
	Given a list of ints >=0 and <=255, MSB first, shift and add and return
	the resulting integer.
	"""

	assert all(map(lambda x: x > -1 and x < 256, l))

	return reduce(lambda x,y: (x<<8)+y, l)

def _int_to_ints(i, n=1):

	"""
	Given an integer, return a list of 8-bit integers (bytes) representing
	it, MSB first.  Pad to n bytes.  Surely there is a better way...
	"""

	assert i > -1

	bpw = 8

	desired_length = n*bpw

	format_string = '{{:0{}b}}'.format(desired_length)

	s = format_string.format(i)

	r = []

	for i in range(n):

		r.append(int(s[i*bpw:(i+1)*bpw], base=2))

	if i>=0:

		return r

	else:

		assert abs(i) <= 2**(desired_length-1), "Negative number is would clobber sign first bit (twos-compliment sign bit)"

		return [~e for e in r]

def _fixedpoint_to_speed(i, fb=28, tick_duration=250e-9):

	"""
	Given a speed in steps per tick as an integer representing a fixed-point
	number, return a speed in steps/s.
	"""

	return (i*2**-fb)/tick_duration

def _fixedpoint_to_acc(i, fb=40, tick_duration=250e-9):

	"""
	Given an acceleration in steps per tick per tick as an integer representing
	a fixed-point number, return a speed in steps/s.
	"""

	return (i*2**-fb)/tick_duration**2

def _speed_to_fixedpoint(steps_per_second, fb=28, tick_duration=250e-9):

	"""
	Given an unsigned speed in units of steps per second, returns as a
	fixed-point number of a given number of fractional bits as an integer.
	Hint: For values larger than what will fit in one byte i.e. >255, pass the
	output of this function to _int_to_ints.
	"""

	# sec     steps     
	# ----- * ------ =  speed * tick_duration = step/tick
	# tick	  sec

	r = int(tick_duration * steps_per_second * 2**fb)

	return r

def _acc_to_fixedpoint(steps_per_second_squared, fb=40, tick_duration=250e-9):

	"""
	Given an acceleration in units of steps per second squared, returns a
	fixed-point number of a given number of fractional bits as an integer.
	Hint: For values larger than what will fit in one byte, pass the output of
	this function to _int_to_ints.
	"""

	# sec    sec    step
	# ---- * ---- * ---- = acc * tick_duration**2 = step/tick**2
	# tick	 tick   s**2

	return int(tick_duration**2 * steps_per_second_squared * 2**fb)

def _unpackint(i):

	"""
	Get the lowest 8 bits of an int.  Returns a list of 8 integers of value 0 or
	1.  This is effectively the inverse of _bits_to_int.

	Example:
	In [72]: L6470._unpackint(42)
	Out[72]: [0, 0, 1, 0, 1, 0, 1, 0]

	In [73]: 0b00101010
	Out[73]: 42
	"""

	assert type(i) is int

	assert i >= 0
	assert i <= 255

	return map(lambda x: i >> x & 1, reversed(range(8)))

def _unpackints(a):

	"""
	Get the bit values from a list of integers, return a list of all bits
	MSB first.

	Example:
	In [74]: L6470._unpackints([42, 13])
	Out[74]: [0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1]
	"""

	return reduce(lambda x,y: x+y, map(_unpackint, a))

class connection():

	"""
	Instances of this class provide an SPI connection to an ST Micro L6470
	stepper motor control IC from a Beaglebone Black.  Methods include Run,
	Move, SoftHiZ.

	Example:
	import L6470
	c = L6470.connection() # /dev/spidev1.0
	"""

	def __init__(self,
		spibus=0,
		spidevice=0,
		reset_pin='P8_10',
		Rphase=2.0,
		Lphase=0.015,
		Irated=2.1,
		Vs=12,
		Ke=0.01,
		spr=200,
		tick_duration=250e-9,
		clobber=True,
		Kacc=1.0,
		Kdec=1.0,
		Krun=1.0,
		Khold=1.0):

		"""
		Returns an open SPI device.

		Parameters
		----------
		spibus : 0 or 1
			The BBB SPI bus to use.  Seems to be x-1 from /dev/spidevx.y.

		spidevice : int
			The BBB SPI device to use.  As multiple devices may
			live on one SPI bus, this would seem to provide a way
			to identify the chip select line you want to drive low.
			Use 0 for BBB pin spi0_cs0.  Seems to be y from
			/dev/spidevx.y.

		reset_pin : str
			The BBB GPIO you've wired to the L6470's RESETN pin.
			The device overlay must be setup to allow this pin to
			be used as GPIO.  P8_10 is in GPIO mode by default.

		Rphase : float
			Stepper motor phase resistance in ohms.  Used to calculate KVAL.

		Lphase : float
			Stepper motor phase inductance in henrys.  Used to calculate intersect speed INT_SPEED.

		Irated : float
			Motor rated phase current in amps.  Used to calculate KVAL.

		Vs : float
			Motor supply voltage in volts.  Used to calculate KVAL.

		Ke : float
			Motor constant [V/Hz].  I've determined this value experimentally for
			my motor by Vs/(4x) where x is the fastest speed [step/s] that the
			motor will turn with all BEMF compensation disabled.

		spr : int
			Whole steps per revolution of the motor.  May be used
			to convert real angular speeds to step rates.

		tick_duration : float
			Nominal tick duration of the L6470 in seconds.
		"""

		self.spibus = spibus
		self.spidevice = spidevice
		self.reset_pin = reset_pin
		self.Rphase = Rphase
		self.Lphase = Lphase
		self.Vs = Vs
		self.Irated = Irated
		self.Ke = Ke
		self.spr = spr
		self.tick_duration = tick_duration
		self.Kacc = Kacc
		self.Kdec = Kdec
		self.Krun = Krun
		self.Khold = Khold

		# Parallel thread variables.
		self.monitor = {}
		self.monitor['V'] = Value('d') # Average velocity.
		self.monitor['Vrad'] = Value('d') # Average velocity.
		self.monitor['p'] = Value('l') # Software position register.
		self.monitor['prad'] = Value('d') # Software position register [rad].
		self.stop = threading.Event() # Average velocity stopper event: stop.set() to stop.
		self.stoptrimmer = threading.Event() # Average velocity stopper event: stop.set() to stop.
		self.monitor['e'] = Value('d') # Position error while tracking.
		self.spilock = threading.Lock() # acquire this Lock before doing anything with SPI.
		self.poffset = 0
		self.poffsetrad = 0

		"""
		Set up the SPI connection.
		"""
		self.spi = SPI(spibus,spidevice)
		self.spi.msh = 5 * 10**6 # SPI clock [Hz]
		self.spi.bpw = 8
		self.spi.threewire = False
		self.spi.lsbfirst = False
		self.spi.mode = 3
		self.spi.cshigh = False
		self.spi.open(spibus,spidevice)

		self.is_closed = False


		# Define a function containing an infinite loop and which is
		# meant to run in a parallel thread of execution and which
		# takes a stepper object "o" and keeps periodically-sampled
		# values from which a moving average is used to update a public
		# shared-memory value.
		def logger(o):

			from collections import deque

			"""
			Create a queue object and start a process to periodically log the
			current time and position.  The result is an average
			over the last dt*ql seconds.

			dt : float
				cycle time.

			ql : int
				queue length.

			"""

			dt = 0.02
			ql = 80

			sd = o.step_divisor

			# A queue of (time, position) tuples which expects elements to move
			# left to right i.e.  use only appendleft and pop.
			q = deque([], maxlen=ql)

			# Infinite queue-maintenance loop.
			while not o.stop.is_set():

				# Is the queue full?
				full = len(q) >= ql

				# Get the current time and [software] position.
				t = time.time()
				p = o.current_spos
				prad = o.current_sposrad

				# Don't remove any elements until the queue is full.
				if full:

					# Pop the oldest (time,pos) tuple off the RHS i.e. the end.
					t0,p0 = q.pop()

					# Detect L6470 position register
					# overflow based on an unrealistic
					# change in position between the last
					# value and now.  If so, recalculate p
					# and dp based on the new value of
					# poffset.
					dp = p - q[0][1]
					if dp>500000:

						print "poffset decreased"

						if o.current_aspeed<0:
							
							o.poffset -= 2**22
							o.poffsetrad -= 2**22/o.step_divisor/o.spr*2*pi

						t = time.time()
						p = o.current_spos
						prad = o.current_sposrad

					elif dp<-500000:

						print "poffset increased"

						if o.current_aspeed>0:
						
							o.poffset += 2**22
							o.poffsetrad += 2**22/o.step_divisor/o.spr*2*pi

						t = time.time()
						p = o.current_spos
						prad = o.current_sposrad

					# Update the software position
					# register.  As opposed to
					# .current_spos, this only updates
					# discretely but it should always be
					# consistent with the current value of
					# poffset.
					o.monitor['p'].value = p
					o.monitor['prad'].value = prad

					# Compute the average velocity [whole
					# step/s] between now and that old
					# tuple and update the moving average
					# via the shared memory object.
					Vbar = (float(p)-p0)/(float(t)-t0)/float(sd)
					o.monitor['V'].value = Vbar
					o.monitor['Vrad'].value = Vbar/o.spr*2*pi

				# Append the current time and position to the
				# left end of the queue.
				q.appendleft((t, p))

				# Wait a while.
				time.sleep(dt)

		# Create a new thread for the logger function to run in, and
		# pass it this L6470 connection object.
		self.p = threading.Thread(target=logger, args=(self,))

		# Start the loop going.  self.monitor['V'].value should change over
		# time henceforth.
		self.p.start()

	def __del__(self):

		if not self.is_closed:

			self.close()

	def close(self):

		"""
		Once a connection object is closed, it may not be used again
		and a new connection must be instantiated.
		"""

		if self.is_closed:

			raise Exception('This connection is already closed.')

		self.stop.set() # raise logger thread stop flag
		self.stoptrimmer.set() # raise trimmer thread stop flag

		if hasattr(self, 'ptrimmer'):

			self.ptrimmer.join() # join any trimmer thread.

		self.p.join() # join the logger thread.

		self.spi.close()

		self.is_closed = True

	def _reset_by_pin(self, reset_delay=0.2):

		"""
		Reset the device by briefly driving the RESETN pin low.
		"""


		# Drive RESETN low to reset L6470.
		GPIO.setup(self.reset_pin, GPIO.OUT)
		GPIO.output(self.reset_pin, GPIO.LOW)
		time.sleep(reset_delay)
		GPIO.output(self.reset_pin, GPIO.HIGH)
		GPIO.cleanup()

	def GetStatus(self, parse=False):

		"""
		Read the STATUS register.
		"""

		resp = []

		with self.spilock:

			# Send the command.
			self._write([_commands['GetStatus']])

			# Read the results.
			r = _unpackints(self._read(2))
		
			#self._write([_commands['GetStatus']])
			#resp.extend(self._read(1))
			#resp.extend(self._read(1))

			#r = _unpackints(resp)

		bits = [
			('SCK_MOD',1),
			('STEP_LOSS_B',1),
			('STEP_LOSS_A',1),
			('OCD',1),
			('TH_SD',1),
			('TH_WRN',1),
			('UVLO',1),
			('WRONG_CMD',1),
			('NOTPERF_CMD',1),
			('MOT_STATUS',2),
			('DIR',1),
			('SW_EVN',1),
			('SW_F',1),
			('BUSY',1),
			('HiZ',1),
			]

		bd = {
			'HiZ': {'0': 'Not HiZ', '1': 'HiZ'},
			'BUSY': {'0': 'Busy', '1': 'Not busy'},
			'SW_F': {'0': 'Switch open', '1': 'Switch closed'},
			'SW_EVN': {'1': 'Switch just closed', '0': 'No switch event'},
			'DIR': {'0': 'Reverse', '1': 'Forward'},
			'MOT_STATUS': {'00': 'Stopped', '01': 'Accelerating', '10': 'Decelerating', '11': 'Constant speed'},
			'NOTPERF_CMD': {'1': 'Command not performed', '0': 'Command performed'},
			'WRONG_CMD': {'1': 'No such command', '0': 'Command recognized'},
			'UVLO': {'0': 'Undervoltage lockout', '1': 'No undervoltage lockout'},
			'TH_WRN': {'0': 'Thermal warning', '1': 'No thermal warning'},
			'TH_SD': {'0': 'Thermal shutdown', '1': 'No thermal shutdown'},
			'OCD': {'0': 'Overcurrent detected', '1': 'Overcurrent not detected'},
			'STEP_LOSS_A': {'0': 'Step loss on coil A', '1': 'No step loss on coil A'},
			'STEP_LOSS_B': {'0': 'Step loss on coil B', '1': 'No step loss on coil B'},
			'SCK_MOD': {'1': 'Step clock mode active', '0': 'Step clock mode not active'},
		}

		pos = 0

		rr = []

		for (name,length) in bits:

			rr.append((name,r[pos:pos+length]))

			pos += length

		if parse:

			return {k:bd[k][''.join(map(str,v))] for (k,v) in rr}

		else:

			return {k:''.join(map(str,v)) for (k,v) in rr}

	@property
	def step_divisor(self):

		"""
		Returns the current microstep setting [ustep/step].
		"""

		# The 3 LS bits of the STEP_MODE register are log2(the step divisor).
		sdp = 0b111 & _bits_to_int(self.GetParam(param='STEP_MODE'))

		return 2**sdp

	def Run(self, rate, units='step/s', block=False):

		"""
		Set the target speed.  An accurate clock is assumed.  Compensation for
		the present value of STEP_SEL is automatic.  Direction is determined by
		sign.  The minimum settable speed is 0.015 step/s.

		Parameters
		----------
		rate : float
			The numerical value of the desired rate of rotation.

		units : str
			The units of the desired rate of rotation.  Must be one of
			['step/s', 'ustep/s', 'rad/s', 'rev/min', 'RPM', 'deg/s', 'arcmin/s',
			'arcsec/s', 'rev/s'].

		block : bool
			If True, block until the L6470 reports that it has
			reached its target.
		"""

		assert units in ['step/s', 'ustep/s', 'rad/s', 'rev/min', 'RPM', 'deg/s',
				'arcmin/s', 'arcsec/s', 'rev/s']

		if rate >= 0:

			dir = _DIR['Forward']

		else:

			dir = _DIR['Reverse']

		if units == 'step/s':
		
			steps_per_second = abs(rate)

		elif units == 'ustep/s':

			steps_per_second = abs(rate)/self.step_divisor

		elif units == 'rad/s':

			steps_per_second = abs(rate)/2./pi*self.spr

		elif units in ['rev/min', 'RPM']:

			steps_per_second = abs(rate)*self.spr/60.0

		elif units == 'deg/s':

			steps_per_second = abs(rate)*self.spr/360.0

		elif units == 'arcmin/s':

			steps_per_second = abs(rate)*self.spr/360.0/60.

		elif units == 'arcsec/s':

			steps_per_second = abs(rate)*self.spr/360.0/60.0/60.0

		elif units == 'rev/s':

			steps_per_second = abs(rate)*self.spr

		else:

			raise Exception('Unknown units.')

		with self.spilock:
	
			self._write([_commands['Run'] + dir] + _int_to_ints(_speed_to_fixedpoint(steps_per_second, fb=28), n=3))

		if block:
			
			while self.GetStatus(parse=True)['BUSY'] == 'Busy':

				time.sleep(0.005)

	def RunTrimmed(self, rate):

		"""
		Closed-loop-in-software variant of Run.

		Control the set speed [whole step/s] in a parallel loop to
		minimize position error from a constant-velocity target.
		Creates a new member of self.monitor, "e", to represent the
		difference between current position and ideal position a.k.a
		the error.

		Parameters
                ----------
                rate : float
                	Target speed [whole step/s].

		"""

		# Clean up from a previously-run trimmer if one exists.
		if hasattr(self, 'ptrimmer'):

			self.stoptrimmer.set()

			while self.ptrimmer.isAlive():

				time.sleep(0.1)

			self.ptrimmer.join()
		

		self.stoptrimmer.clear()

		self.Run(rate, block=True)

		rate = float(rate)

		self.monitor['e'].value = 0.0

		# A function including an infinite loop meant to be run in a
		# parallel thread ad infinitum to control speed to minimize
		# tracking error.
		def const_vel_trimmer(o):

			# Start time and position [whole steps].
			t0 = time.time()
			#p0 = float(o.monitor['p'].value)/o.step_divisor
			p0 = o.monitor['p'].value/o.step_divisor

			def gett():
				
				# Return seconds since start.

				return time.time() - t0

			def p():

				"""
				Current position [whole steps].  Returns a float.
				"""

				return float(o.monitor['p'].value)/o.step_divisor - p0

			def pt(t):

				"""
				Target position [whole steps] at seconds since start t.  Returns a float.
				"""

				return rate*t

			vt = rate

			st = 0.1 # cycle time [s]
			ki = 0.01
			tdt = 2 # target make-up time [s]

			# Initialize loop values.
			e = 0
			eold = 0
			told = gett()
			posold = 0
			integral = 0

			# Set the flat to stop this loop.
			while not o.stoptrimmer.is_set():

				# Current loop position and target position.
				t = gett()
				pos = p()
				post = pt(t)

				dt = t-told # age of "old" values

				Vmeasured = (pos-posold)/dt

				# Current position error.  Positive means we're
				# behind, negative ahead.
				e = post - pos

				# Increment the error integral.
				integral += e*dt

				# Update the public shared memory position error value.
				o.monitor['e'].value = e

				# Underrelaxation.
				#urf = 0.5
				#vt = urf*(rate + e/tdt + integral*ki) + (1-urf)*vt
				vt = rate + e/tdt + integral*ki

				def sign(x):

					if x>=0:

						return 1

					else:

						return -1

				# Command the new speed.
				o.Run(vt)

				#print '{:.4f}\t{:.6f}\t{:6f}\t{:16d}'.format(e, vt, Vmeasured, o.current_aspeed_int)
				told = t
				eold = e
				posold = pos

				time.sleep(st)


		# Create a new thread for the trimmer function to run in, and
		# pass it this L6470 connection object.
		self.ptrimmer = threading.Thread(target=const_vel_trimmer, args=(self,))

		# Start the loop going.  self.monitor['e'].value should change
		# over time and the velocity demand should vary henceforth.
		self.ptrimmer.start()


	def RunTrimmedf(self, fpos, pt, frun, gett=None, testing=False):

		"""
		Like RunTrimmed but more versatile.

		Parameters
		----------
		fpos : func
			Takes no arguments and must return the current position.

		pt : func
			Must take one argument (a float, time since "the start"
			[s]) and return the target position in units consistent
			with fpos.

		frun : func
			Must take one argument (a float, the target velocity)
			and set the speed.

		gett : func

			Takes no arguments and returns the current time consistent with pt.
		
		"""

		# Clean up from a previously-run trimmer if one exists.
		if hasattr(self, 'ptrimmer'):

			self.stoptrimmer.set()

			while self.ptrimmer.isAlive():

				time.sleep(0.1)

			self.ptrimmer.join()

		self.stoptrimmer.clear()

		self.monitor['e'].value = 0.0

		def const_vel_trimmer(o):

			"""
			A function including an infinite loop meant to be run
			in a parallel thread ad infinitum to control speed to
			minimize tracking error.

			Parameters
			----------
			o : connection
			"""

			# Start time and position [whole steps].
			t0 = time.time()
			#p0 = fpos()

			#if gett == None:
			#
			#	def gett():
			#		
			#		# Seconds since start.
			#	
			#		return time.time() - t0

			def p():

				"""
				Current position.
				"""

				return fpos()
				#return fpos() - p0

			def dpdt(t):

				"""
				Estimate the target velocity using a finite
				difference of the target position.
				"""

				dti = 0.1

				return (pt(dti+t)-pt(t))/dti

			st = 0.5 # cycle time [s]
			kp = 1.0
			ki = 0.01
			tdt = 4.0 # target make-up time [s]

			# Initialize loop values.
			vt = 0
			e = 0
			eold = 0
			t = told = gett()
			posold = 0
			integral = deque([], maxlen=20)

			# To start, drive to an initial speed equal to the
			# first derivative of the target position function.
			dti = 0.1
			self.Run((pt(dti+t)-pt(t))/dti)

			while self.GetStatus(parse=True)['BUSY'] == 'Busy':

				time.sleep(0.005)

			counter = 0

			# NB: Call <connection>.stoptrimmer.set() to stop this loop.
			while not o.stoptrimmer.is_set():

				# Current loop position and target position.
				t = gett()
				pos = p()
				post = pt(t)

				dt = t-told # age of "old" values

				Vnom = dpdt(t)

				Vmeasured = (pos-posold)/dt

				# Current position error.  Positive means we're
				# behind, negative ahead.
				e = post - pos

				# Increment the error integral.
				integral.append(e*dt)

				# Update the public shared memory position error value.
				o.monitor['e'].value = e

				# Underrelaxation.
				#urf = 0.1
				#vt = urf*(rate + e/tdt + integral*ki) + (1-urf)*vt
				#vt = urf*(Vnom + kp*e/tdt + integral*ki) + (1-urf)*vt
				pterm = kp*e/tdt
				iterm = sum(integral)*ki
				vt = Vnom + pterm + iterm

				def sign(x):

					if x>=0:

						return 1

					else:

						return -1

				fmer = '{:14.8f}'
				fmers = '{:>14}'
				ffields = ["pos", "e", "Vnom", "pterm", "iterm", "vt", "Vmeasured", "o.current_aspeed_int"]
				fpolymers = ''.join([fmers]*8)
				fpolymer =  ''.join([fmer ]*8)
				if counter == 0:

					#print fpolymers.format(*ffields)

					counter = 20

				#print fpolymer.format(*map(eval, ffields))

				# Command the new speed.
				if not testing:
					frun(vt)

				told = t
				eold = e
				posold = pos

				counter -= 1

				time.sleep(st)


		# Create a new thread for the trimmer function to run in, and
		# pass it this L6470 connection object.
		self.ptrimmer = threading.Thread(target=const_vel_trimmer, args=(self,))

		# Start the loop going.  self.monitor['e'].value should change
		# over time and the velocity demand should vary henceforth.
		self.ptrimmer.start()





	def Move(self, x=1, block=False, units='ustep'):

		"""
		Move so many microsteps from the current position according to
		the current value of STEP_SEL.
		"""

		assert units in ['ustep', 'step', 'rev', 'rad']

		if units == 'rev':

			usteps = x*self.spr*self.step_divisor

		elif units == 'rad':

			usteps = x*self.spr*self.step_divisor/2./pi

		elif units == 'step':

			usteps = x*self.step_divisor

		else:

			usteps = x

		usteps = int(usteps)

		if usteps >= 0:

			dir = _DIR['Forward']

		else:

			dir = _DIR['Reverse']
	

		self._write([_commands['Move']+dir] + _int_to_ints(abs(usteps), n=3))

		if block:
			
			while self.GetStatus(parse=True)['BUSY'] == 'Busy':

				time.sleep(0.005)

	def GoTo(self, usteps=0):

		"""
		Go to a position in terms microsteps according to the current
		value of STEP_SEL by the shortest path relative to ABS_POS's horizon of
		+/-2**21.  Use GoTo_DIR to enforce CW or CCW rotation.
		"""

		if usteps>=0:

			self._write([_commands['GoTo']] + _int_to_ints(usteps, n=3))

		else:

			assert abs(usteps) <= 2**(22-1), ("Negative number would "
				"clobber sign first bit (twos-compliment sign bit)")

			self._write([_commands['GoTo']] + [~e for e in _int_to_ints(abs(usteps)-1, n=3)])

	def GoTo_DIR(self, usteps=0, dir='Forward'):

		"""
		Go to a position in terms microsteps according to the current
		value of STEP_SEL moving in the direction given.
		"""

		assert dir in ['Forward', 'Reverse']

		if usteps>=0:

			self._write([_commands['GoTo_DIR']+_DIR[dir]] + _int_to_ints(usteps, n=3))

		else:

			assert abs(usteps) <= 2**(22-1), ("Negative number would "
				"clobber sign first bit (twos-compliment sign bit)")

			self._write([_commands['GoTo_DIR']+_DIR[dir]] + [~e for e in _int_to_ints(abs(usteps)-1, n=3)])

	def ResetDevice(self):

		"""
		Send the AutoDriver chip the "Reset" command over SPI.
		"""

		with self.spilock:
	
			self._write([_commands['ResetDevice']])

	def _read(self, n=1):

		"""
		Similar to writing, the Adafruit_BBIO.SPI functions readbytes, xfer, and
		xfer2 don't work for more than 1 byte at a time.  Read n bytes 1 at a
		time and concatenate the result.

		Parameters
		----------
		n : int
			Number of bytes to read.

		Returns
		-------
		list of ints
		"""

		x = []

		for i in range(n):

			x.append(self.spi.readbytes(1)[0])
	
		return x

	def _write(self, x):

		"""
		Write a list of 1 or more bytes via SPI.  Adafruit_BBIO.SPI functions
		writebytes, xfer, and xfer2 don't work for more than 1 byte at a time.
		Clock out one byte at a time.

		Parameters
		----------
		x : list of ints in the range [0,255].
		"""

		for e in x:

			self.spi.writebytes([e])


	def GetParam(self, param='ABS_POS', n=1):

		with self.spilock:

			# Send the command.
			self._write([0b00100000 + _address[param]])

			# Read the results.
			return _unpackints(self._read(n))


	def SetParam(self, param='ABS_POS', value=[0]):

		with self.spilock:

			self._write([_address[param]] + value)

	def ResetPos(self):

		"""
		Set ABS_POS = HOME = 0.
		"""

		with self.spilock:

			self._write([_commands['ResetPos']])

	def SoftHiZ(self, block=False):

		"""
		Ramp speed down then power the bridges off.
		"""

		with self.spilock:

			self._write([_commands['SoftHiZ']])

		if block:
			
			while self.GetStatus(parse=True)['BUSY'] == 'Busy':

				time.sleep(0.005)

	def HardHiZ(self):
		
		"""
		This immediately powers the bridges off putting them in a
		high-impedence state giving zero torque.  If speed != 0 and
		inertia != 0, the load will coast.
		"""

		with self.spilock:

			self._write([_commands['HardHiZ']])

	def _HardStop(self):

		"""
		This is the harshest stop.  Powered.  Immediate.
		"""
		with self.spilock:

			self._write([_commands['HardStop']])

	def SoftStop(self, block=False):

		"""
		Ramp speed down then hold.
		"""
		with self.spilock:

			self._write([_commands['SoftStop']])

		if block:
			
			while self.GetStatus(parse=True)['BUSY'] == 'Busy':

				time.sleep(0.005)

	def set_ustep(self, sdp=1):

		"""
		Divide whole steps into 2**sdp steps.

		sdp : int
			"Step-division power" :divide whole steps into 2**sdp
			steps.
		"""

		assert sdp in range(8)

		self.SoftHiZ()

		self.SetParam(param='STEP_MODE', value=[0b00000000 + sdp])

		while self.GetStatus(parse=True)['BUSY'] == 'Busy':

			time.sleep(0.005)

	@property
	def current_pos(self):

		"""
		Returns the current absolute electrical position in microsteps
		in terms of the current value of STEP_SEL.  Changes to STEP_SEL
		will render ABS_POS meaningless.
		"""

		# Absolute position is stored in twos-compliment format in 22
		# bits, giving values between -2**21 and 2**21-1.
		three_bytes = self.GetParam('ABS_POS', 3)

		sign = {1: -1, 0: 1}[three_bytes[2]]

		mag = three_bytes[-21:]

		if sign == 1:

			return _bits_to_int(mag)

		else:

			return -2**21 + _bits_to_int(mag)

	@property
	def current_posrad(self):

		"""
		Returns the current absolute position in radians from the
		origin.
		"""

		# Absolute position is stored in twos-compliment format in 22
		# bits, giving values between -2**21 and 2**21-1.
		three_bytes = self.GetParam('ABS_POS', 3)

		sign = {1: -1, 0: 1}[three_bytes[2]]

		mag = three_bytes[-21:]

		if sign == 1:

			return 1.0*_bits_to_int(mag)/self.step_divisor/self.spr*2*pi

		else:

			return (-2.0**21 + _bits_to_int(mag))/self.step_divisor/self.spr*2*pi

	@property
	def current_spos(self):
		
		"""
		Calculate the current position [usteps] as the sum of the
		L6470's position register and the software-maintained offset.

		WARNING: since the poffset and current_pos will occasionally
		be out of sync due to poffset being updated in a separate
		thread, this function's return value may change
		discontinuously.  Use c.monitor['p'] for a value guaranteed to
		move continuously albeit one which is updated at a slower
		rate.
		"""

		return self.current_pos + self.poffset

	@property
	def current_sposrad(self):
		
		"""
		Like current_spos but [radians] not [usteps].
		"""

		return self.current_posrad + self.poffsetrad

	@property
	def current_aspeed(self):

		"""
		Returns the current electrical speed in units of whole steps
		per second regardless of stepsize *according to the chip and
		assuming an L6470 tick of 250 ns*.
		"""

		integer = _bits_to_int(self.GetParam('SPEED', 3))

		dir = {'1': 1, '0': -1}[self.GetStatus()['DIR']]
		mag = _fixedpoint_to_speed(integer)

		return dir*mag

	@property
	def current_aspeed_int(self):

		"""
		Returns the contents of the L6470's SPEED register i.e. the
		electrical speed according to the chip and assuming an L6470
		tick of 250 ns.
		"""

		integer = _bits_to_int(self.GetParam('SPEED', 3))

		return integer


	@property
	def current_speed(self):

		"""
		Get the moving average step rate.  The time basis is the
		system clock.
		"""

		return self.monitor['V'].value

	@property
	def clockfactor(self):

		"""
		Return the ratio of current measured speed (assuming an
		accurate system clock) to current demanded speed (the value in
		the L6470's SPEED register) i.e. 1 over the L6470's oscillator
		frequency error.  In situations where the value would be bogus,
		this function returns None.
		"""

		cs = self.current_speed
		cas = self.current_aspeed

		if cas != 0:

			try:

				return cs/cas

			except ZeroDivisionError:

				return None

		else:

			return None

	def responsive(self):

		"""
		Confirm that the L6470 is responsive.  Returns a boolean.
		"""

		# Read the value of the MAX_SPEED register so we can put it
		# back.  The register is only 10 bits long.
		current_ms_register = self.GetParam('MAX_SPEED', n=2)[-10:]

		# Write a different value to the MAX_SPEED register.  MAX_SPEED
		# is a good candidate as it is always writable.  The register
		# is 10 bits long.
		value_to_check = [0b10, 0b10101010]
		self.SetParam('MAX_SPEED', value_to_check) 

		# Again, the register is 10 bits long.
		if _bits_to_int(self.GetParam('MAX_SPEED', n=2)[-10:]) == _bytes_to_int(value_to_check):

			"""
			Evidently the chip is responsive.
			"""

			value_to_restore = _int_to_ints(_bits_to_int(current_ms_register))

			# Zero-pad to 2 bytes long.
			while len(value_to_restore)<2:
				value_to_restore = [0]+value_to_restore

			# Restore the original value.
			self.SetParam('MAX_SPEED', value_to_restore)

			return True

		else:

			"""
			Evidently the chip is not responsive.
			"""

			# Try to restore the original value anyway.
			self.SetParam('MAX_SPEED', _int_to_ints(_bits_to_int(current_ms_register), n=2))

			return False

	def tune(self):

		"""
		Set some reasonable tuning parameters.
		"""

		# First, some of the commands below may not stick when the
		# motor is running or the bridges are powered.
		self.SoftHiZ(block=True)

		self.ResetDevice()

		# Compute the fraction of the supply voltage required to drive
		# one phase at rated current: 100%=255, 0%=0.
		self.KVAL = int(min(1.0, 1.0*self.Irated*self.Rphase/self.Vs) * 255)

		# Compute the resonant stepping speed.  Appears variously as
		# INT_SPEED and INT_SPD.
		self.INT_SPD = 4*self.Rphase/(2*pi*self.Lphase)

		# Over current detection threshold: 16 = 6 A, 0 = 0.375 A.
		self.SetParam('OCD_TH', [8])

		self.SetParam('INT_SPD', _int_to_ints(_speed_to_fixedpoint(self.INT_SPD, fb=24), n=2))

		SLP_unit = 0.000015 # fraction/(step/s)

		self.SetParam(param='ST_SLP', value=[int(self.Ke/4/self.Vs/SLP_unit)])
		self.SetParam(param='FN_SLP_ACC', value=[int(1.5*self.Ke/4/self.Vs/SLP_unit)])
		self.SetParam(param='FN_SLP_DEC', value=[int(1.5*self.Ke/4/self.Vs/SLP_unit)])
		#self.SetParam(param='FN_SLP_ACC', value=[int((2*pi*self.Lphase*self.Irated + self.Ke)/4/self.Vs/SLP_unit)])
		#self.SetParam(param='FN_SLP_DEC', value=[int((2*pi*self.Lphase*self.Irated + self.Ke)/4/self.Vs/SLP_unit)])

		# Set minimum controllable speed to 0.
		low_speed_optimization = True
		self.SetParam(param='MIN_SPEED', value=[0b00000000 + (low_speed_optimization<<4), _speed_to_fixedpoint(30, fb=24)])

		# Set max speed.
		self.SetParam(
			param='MAX_SPEED',
			value=_int_to_ints(_speed_to_fixedpoint(300, fb=18), n=2))

		# Set speed above which full steps are used.
		self.SetParam(
			param='FS_SPD',
			value=_int_to_ints(_speed_to_fixedpoint(200, fb=18), n=2))

		# Set ramp rates.
		self.SetParam(
			param='ACC',
			value=_int_to_ints(_acc_to_fixedpoint(300, fb=40), n=2))

		self.SetParam(
			param='DEC',
			value=_int_to_ints(_acc_to_fixedpoint(300, fb=40), n=2))

		# Set microstep size.  One step is divided into two raised to
		# the power of the value passed.  Enable the BUSYN pin output
		# to indicate busy.
		self.set_ustep(7)
		
		# Set the two-byte CONFIG register.
		# MSByte: F_PWM_INT (3 bits), F_PWM_DEC (3), POW_SR (2)
		# LSByte: OC_SD, reserved, EN_VSCOMP, SW_MODE, EXT_CLK, OSC_SEL(3)
		self.SetParam(param='CONFIG', value=[
			(_F_PWM_INT[1]<<5) + (_F_PWM_DEC[2]<<2) + _POW_SR[530],
			(_OC_SD['enable over-current shutdown']<<7) +\
			(_EN_VSCOMP['disable supply voltage variation compensation']<<5) +\
			(_SW_MODE['User-defined']<<4) +\
			(_EXT_CLK_and_OSC_SEL['external 16 MHz crystal'])])

		# Set regime-specific voltage limits.  value/256 is the
		# fraction of VS available to the coils while accelerating,
		# deceleration, running at constant speed, and holding
		# position, respectively.
		self.SetParam(param='KVAL_ACC', value=[int(self.KVAL*self.Kacc)])
		self.SetParam(param='KVAL_DEC', value=[int(self.KVAL*self.Kdec)])
		self.SetParam(param='KVAL_RUN', value=[int(self.KVAL*self.Krun)])
		self.SetParam(param='KVAL_HOLD', value=[int(self.KVAL*self.Khold)])
