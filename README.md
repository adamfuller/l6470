# Overview #
This is a Python module for communicating with an [STMicroelectronics L6470 stepper motor control IC](http://www.st.com/web/catalog/sense_power/FM142/CL851/SC1794/SS1498/LN1723/PF248592?sc=internet/analog/product/248592.jsp#) over SPI from a Beaglebone Black running Debian.  Leverage all the power of the L6470, from Python, plus augment the L6470 where it's lacking e.g. absolute positioning and timekeeping, multithreading, objectification.

# Features #
* A multi-threaded object encapsulating all SPI transactions with a L6470: `connection`.
* System-clock-disciplined variants of `Run`: `RunTrimmed` and `RunTrimmedf`, to provide accurate long-run positioning.  Using NTP, the Beaglebone's system clock may be kept accurate to within circa 10 ms of UTC (see 'estimated error' from `ntpdc -c kerninfo`).  We may therefore use the Beaglebone system time (`import time; time.time()`) to discipline the motor speed (via repeated calls to `c.Run`) to minimize the error between the current position and a system-clock-derived target position.  In this way, the long-run positional accuracy may approach that of NTP.  Consider the challenge of running at a constant speed of 1 step/s for 1200 seconds (20 minutes).  Using the L6470 internal oscillator (+/- 3% or 30000 ppm), the actual position may have drifted away from the intended target position by as much as 36 steps in either direction.  Even with a [+/-50 ppm crystal](https://www.sparkfun.com/products/536) you could be out by as much as 0.06 steps, and there is nothing to stop that error from accumulating for longer runs.  Using NTP, your position may be kept within 0.01 steps for as long as the Beaglebone maintains an NTP connection.
* A software position register to extend the L6470's 22-bit absolute position register: `connection.monitor['p'].value`.  In a parallel thread, large steps in the `ABS_POS` register are detected and used to adjust an offset value `poffset` by multiples of `2**2`:  `monitor['p'].value = ABS_POS + poffset`.
* Humanization of the STATUS packet: `connection.GetStatus(parse=True)`.


# A tiny example #
```
#!python

import L6470 # Requires Adafruit_BBIO.
c = L6470.connection() # Instantiate the connection class, default is /dev/spidev1.0.
c.Run(200) # Congratulations, your stepper motor should be turning at approximately 1 rev/s.
c.SoftHiZ() # Stop the motor.
c.close() # Always close the connection object prior to quitting Python.
```


# A more complete example #
```
#!python
import L6470 # Requires Adafruit_BBIO.
c = L6470.connection() # Default is /dev/spidev1.0.
c.ResetDevice() # Works by SPI, see also c._reset_by_pin.
c.tune() # Configure the chip for my motor.
assert c.responsive() # Confirm that a register can be written and read via SPI.
c.GetStatus(parse=True) # Ask the L6470 how it's doing.
c.Run(200) # Confirm motor turns approximately 1 rev/s.
c.SoftHiZ() # Stop the motor.
c.close() # Always close connection objects prior to quitting Python.
```


# To do #
* Compare speed discipline algorithms for `RunTrimmed`, estimate the achievable range of instantaneous error for constant velocity operation over 100s of seconds for the different algorithms, pick one, and optimize its tuning.  Try PID tuned using [this method](https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method).
* Minimize the use of homebrew bit-level functions e.g. `int_to_ints`.
* In `RunTrimmed`, optionally track using sidereal time (1 sidereal day gives one apparent revolution of the stars or ~366 sidereal days per trip around the sun) and solar time (~365 sunrises per trip around the sun, also what NTP and UTC track) whose rates differ by a ratio of approximately 366/365 or ~2000 ppm.  More precisely, the ratio of the duration of one sidereal second to the duration of one solar second at [J2000](https://en.wikipedia.org/wiki/Epoch_(astronomy)#Julian_years_and_J2000) was [`0.997 269 566 329 084`](http://adsabs.harvard.edu/full/1982A%26A...105..359A) which differs from 365/366 beyond the 6th decimal place.
* Clock cheating i.e. using an 8 MHz external crystal but configuring for a 32 MHz crystal.  Why do this?  To increase the speed resolution by a factor of 4 from the default 0.015 step/s to 0.0037 step/s.  I have tested this by using a 16 MHz crystal and configuring for 32 MHz and it seems to run at exactly half speed.  The connection object could use a new parameter to allow all the speed functions to work as expected e.g. `real_clock_freq`.  This would help reduce positional error jitter when tracking a constant speed target using `RunTrimmed` where the speed corrections are on the order of 0.015 step/s.
* Provide support for coordinated movement between multiple L6470's e.g. for robot wheels or tracks, a pan-tilt camera mount, a two-axis telescope mount, or simply `n` whirlygigs you want to turn in precise coordination.


# Important things I learned while getting this working #

1. SPI pin names are confusing.  I spent ages naively thinking the second `0` in the BBB pin name `spi0_d0` meant "output" and the `1` in `spi0_d1` meant "input".  After failing to communicate with the chip, checking and re-checking [Derek Molloy's pinout](https://github.com/derekmolloy/boneDeviceTree/blob/master/docs/BeagleboneBlackP9HeaderTable.pdf) against my wires, the [SPIDEV docs](http://elinux.org/BeagleBone_Black_Enable_SPIDEV#SPI0) alerted me to the fact that the pins can in fact be reconfigured and therefore that 0 didn't *mean* output and 1 didn't *mean* input.  **On my default everything, P9_18 *is named* SPI0_D1 *which has the function of* MOSI.  P9_21 *is named* SPI0_D0 *which has the function of* MISO.**

2. SPI has four modes reflecting the polarity of the clock and chip select lines.  The L6470 wants Mode 3, but the SPIDEV default is Mode 1.  This module takes care of that when instantiating `Adafruit_BBIO.SPI`.  The mode number comes directly from the excellent [Wikipedia article on SPI](https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus#Mode_numbers) and the L6470 manual's "Serial interface" section, but it was another thing to trip over for someone unfamiliar with SPI.

3. SPI device numbers (1 and 2) in Linux don't match BBIO's 0-based 0 and 1.  Just use the SPI0 pins and BBIO's 0 and 0 and you should be fine.

4. The L6470's SPI protocol requires the chip select be released between each byte.  Neither BBIO's `xfer` nor `xfer2` seem to do the trick.  In fact, `xfer` sometimes seems to echo back the bytes that I had sent in, one byte later.  To be honest I didn't bother reading the documentation after finding that simply using `.writebytes([one_int])` or `.readbytes(1)` for writing or reading one byte at at time worked fine.

5. The stated accuracy of the onboard oscillator is only ±3% i.e. don't expect the L6470 to make a very good wall clock out of the box.  Better absolute accuracy of step rates will require an external oscillator.


# Test details #

I've used [Sparkfun's L6470 breakout board](https://www.sparkfun.com/products/11611): the "Autodriver".

My motor is an ex-CNC NEMA34 Sanyo Denki.  Ratings are 2.5V, 2.1A, 200 steps.  Markings are "103-809-0241, IBM P/N 6019885, 6038349-1".  I don't know the coil inductance.

I've tested with Vs=12 V (using an ATX PC power supply) and 24 V (by adding a motorcycle battery in series).  12 V is more than adequate for driving this motor unloaded at moderate speeds.  The highest step rate I can achieve at 12 V after running `c.tune()` is about 350 step/s.  Funny story, at one point, a bug in my `int_to_ints` function meant the values for `ACC` and `MAXSPEED` were *huge*.  The same bug meant when I sent `c.run(100)` it effectively sent something like `c.run(1000000000)`, as the motor accelerated at an incredible theretofore unseen rate.  It shut down before I had time to recognize what was happening and turn power off.  I think it must've been the thermal or overcurrent shutdown working in my favor, because the chip lives!